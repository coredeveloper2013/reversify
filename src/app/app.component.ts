import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    APP_URL = 'http://mediusware.com/demo/revercify';
    title = 'app';
    preview = {
        overview: 1,
        home: 0,
        car : 0,
        insurance : 0,
        loan : 0,
    };

    home = {
        submitted : 0
    };
    car = {
        submitted : 0
    };
    insurance = {
        submitted : 0
    };
    loan = {
        submitted : 0
    };

    goRoot() {
        this.preview.overview = 1;
        this.preview.home = 0;
        this.preview.car = 0;
        this.preview.insurance = 0;
        this.preview.loan = 0;
        this.home.submitted = 0;
        this.car.submitted = 0;
        this.insurance.submitted = 0;
        this.loan.submitted = 0;
    }
    goHome() {
        this.preview.overview = 0;
        this.preview.home = 1;
        this.preview.car = 0;
        this.preview.insurance = 0;
        this.preview.loan = 0;
        this.home.submitted = 0;
        this.car.submitted = 0;
        this.insurance.submitted = 0;
        this.loan.submitted = 0;
    }
    goCar() {
        this.preview.overview = 0;
        this.preview.home = 0;
        this.preview.car = 1;
        this.preview.insurance = 0;
        this.preview.loan = 0;
        this.home.submitted = 0;
        this.car.submitted = 0;
        this.insurance.submitted = 0;
        this.loan.submitted = 0;
    }
    goInsurance() {
        this.preview.overview = 0;
        this.preview.home = 0;
        this.preview.car = 0;
        this.preview.insurance = 1;
        this.preview.loan = 0;
        this.home.submitted = 0;
        this.car.submitted = 0;
        this.insurance.submitted = 0;
        this.loan.submitted = 0;
    }
    goLoan() {
        this.preview.overview = 0;
        this.preview.home = 0;
        this.preview.car = 0;
        this.preview.insurance = 0;
        this.preview.loan = 1;
        this.home.submitted = 0;
        this.car.submitted = 0;
        this.insurance.submitted = 0;
        this.loan.submitted = 0;
    }
}
